/**
 * @file    Circle.cpp
 * @authors <FILL ME IN ACCORDINGLY>
 * @brief   Circle class implementation.
 */

#include "Circle.h"

Circle::Circle() {
    // TODO: Remove this comment and ensure radius is set as per specification.
}

Circle::Circle(const double& circleRadius) {
    // TODO: Remove this comment and ensure radius is set as per specification.
}

double Circle::getRadius() const {
    // TODO: Remove this comment and return the correct value.
    return 0;
}

void Circle::setRadius(const double& circleRadius) {
    // TODO: Remove this comment and ensure radius is set as per specification.
}

double Circle::getArea() const {
    // TODO: Remove this comment and ensure the correct area is computed and 
    // returned accordingly.
    return 0;
}

double Circle::getPerimeter() const {
    // TODO: Remove this comment and ensure the correct perimeter is computed  
    // and returned accordingly.
    return 0; 
}

Circle::~Circle() {
}
