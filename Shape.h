/**
 * @file    Shape.h
 * @authors <FILL ME IN ACCORDINGLY>
 * @brief   An interface for all Shape objects.
 */

#ifndef SHAPE_H
#define SHAPE_H

class Shape
{
public:
    /**
     * Compute the area of this Shape.
     * 
     * @return The area of this Shape is returned.
     */
    virtual double getArea() const = 0;
    
    /**
     * Compute the perimeter of this Shape.
     * 
     * @return The perimeter of this Shape is returned.
     */
    virtual double getPerimeter() const = 0;
    
    /**
     * Shape destructor.
     */
    virtual ~Shape() {}
};

#endif /* SHAPE_H */

