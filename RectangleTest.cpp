/**
 * @file   RectangleTest.cpp
 * @author Jim Daehn
 * @brief  Implementation of Rectangle Unit Test. DO NOT MODIFY THE CONTENTS 
 * OF THIS FILE! ANY MODIFICATION TO THIS FILE WILL RESULT IN A GRADE OF 0 FOR
 * THIS LAB!
 */

#include "RectangleTest.h"
#include "Rectangle.h"

CPPUNIT_TEST_SUITE_REGISTRATION(RectangleTest);

RectangleTest::RectangleTest() {
}

RectangleTest::~RectangleTest() {
}

void RectangleTest::setUp() {
}

void RectangleTest::tearDown() {
}

void RectangleTest::testDefaultRectangleLength() {
    Rectangle rectangle;
    double actual = rectangle.getLength();
    std::string message = "actual should equal expected";
    double expected = 2.0;
    double delta = 0.000001;
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expected, actual, delta);
}

void RectangleTest::testDefaultRectangleWidth() {
    Rectangle rectangle;
    double actual = rectangle.getWidth();
    std::string message = "actual should equal expected";
    double expected = 1.0;
    double delta = 0.000001;
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expected, actual, delta);
}

void RectangleTest::testDefaultRectangleArea() {
    Rectangle rectangle;
    double actual = rectangle.getArea();
    std::string message = "actual should equal expected";
    double expected = 2.0;
    double delta = 0.000001;
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expected, actual, delta);
}

void RectangleTest::testDefaultRectanglePerimeter() {
    Rectangle rectangle;
    double actual = rectangle.getPerimeter();
    std::string message = "actual should equal expected";
    double expected = 6.0;
    double delta = 0.000001;
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expected, actual, delta);
}

void RectangleTest::testMutatorSetsDefaultLengthGivenNegativeInput() {
    Rectangle rectangle;
    rectangle.setLength(-2.3);
    std::string message = "actual should equal default length of 2.0";
    double actual = rectangle.getLength();
    double expected = 2.0;
    double delta = 0.000001;
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expected, actual, delta);
}

void RectangleTest::testMutatorSetsDefaultWidthGivenNegativeInput() {
    Rectangle rectangle;
    rectangle.setWidth(-2.3);
    std::string message = "actual should equal default width of 1.0";
    double actual = rectangle.getWidth();
    double expected = 1.0;
    double delta = 0.000001;
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expected, actual, delta);
}

void RectangleTest::testParameterizedRectangleArea() {
    double length = 3.0;
     double width = 2.0;
    Rectangle rectangle(length, width);
    double actual = rectangle.getArea();
    std::string message = "actual should equal expected";
    double expected = length * width;
    double delta = 0.000001;
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expected, actual, delta);
}

void RectangleTest::testParameterizedRectanglePerimeter() {
    double length = 3.0;
    double width = 2.0;
    Rectangle rectangle(length, width);
    double actual = rectangle.getPerimeter();
    std::string message = "actual should equal expected";
    double expected = 2.0 * (length + width);
    double delta = 0.000001;
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expected, actual, delta);
}
