/**
 * @file   RectangleTest.cpp
 * @author Jim Daehn
 * @brief  Implementation of Circle Unit Test. DO NOT MODIFY THE CONTENTS 
 * OF THIS FILE! ANY MODIFICATION TO THIS FILE WILL RESULT IN A GRADE OF 0 FOR
 * THIS LAB!
 */

#include "CircleTest.h"
#include "Circle.h"

CPPUNIT_TEST_SUITE_REGISTRATION(CircleTest);

CircleTest::CircleTest() {
}

CircleTest::~CircleTest() {
}

void CircleTest::setUp() {
}

void CircleTest::tearDown() {
}

void CircleTest::testDefaultCircleRadius() {
    Circle circle;
    double actual = circle.getRadius();
    std::string message = "The default radius is expected to be 1.0";
    double expected = 1.0;
    double delta = 0.000001;
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expected, actual, delta);
}

void CircleTest::testDefaultCircleArea() {
    Circle circle;
    double actual = circle.getArea();
    std::string message = "The default area is expected to equal PI";
    double expected = PI;
    double delta = 0.000001;
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expected, actual, delta);
}

void CircleTest::testDefaultCirclePerimeter() {
    Circle circle;
    double actual = circle.getPerimeter();
    std::string message = "The default perimeter is expected to equal 2 * PI";
    double expected = 2 * PI;
    double delta = 0.000001;
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expected, actual, delta);
}

void CircleTest::testMutatorSetsDefaultRadiusGivenNegativeInput() {
    Circle circle;
    circle.setRadius(-2.3);
    double actual = circle.getRadius();
    std::string message = "The radius is expected to default to 1.0";
    double expected = 1.0;
    double delta = 0.000001;
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expected, actual, delta);
}

void CircleTest::testParameterizedCircleArea() {
    double radius = 2.5;
    Circle circle(radius);
    double actual = circle.getArea();
    double expected = PI * radius * radius;
    std::string message = "The actual area does not equal the expected area";
    double delta = 0.000001;
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expected, actual, delta);
}

void CircleTest::testParameterizedCirclePerimeter() {
    double radius = 2.5;
    Circle circle(radius);
    double actual = circle.getPerimeter();
    double expected = 2 * PI * radius;
    std::string message = "The actual perimeter does not equal the expected perimeter";
    double delta = 0.000001;
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expected, actual, delta);
}
