# CSC232 - Data Structures with C++

## Lab 1 - Introduction to Inheritance and Unit Testing in C++

## Background

A recently hired developer, who had quite the reputation of producing extremely well-documented code, has just quit. Before he left, he was working on some code to be used for a graphics program. He was just beginning to develop a Shape class hierarchy. At this point, he had established the basic Shape interface, and in the spirit of Test Driven Development (TDD), he has already written a complete test suite using the CPPUNIT test framework for the two implementations of the Shape interface (Rectangle and Circle). As mentioned, he was extremely good at documenting his work. The complete specifications for this Shape interface and its implementations is found in the source files. However, we need to get this code implemented and reviewed by midnight Friday, September 2, 2016 to meet our current stakeholder's needs. Working in our class pair-programming process, pair up with your programming partner and complete this work by the due date!

## Getting Started

### Fork from Bitbucket.org

If you're not reading this README directly online, open your favorite web browser and visit https://bitbucket.org/professordaehn/msu-csc232-lab01. If you haven't logged into bitbucket yet, do so now. Now that you're logged into your account, you can form the lab repo into your own space by clicking on the "Fork" hyperlink found on the left-hand side of the webpage. When you fork this repo, make sure you set the access level of your fork to "private," that is, place a check mark in the check box that says "This is a private repository". Pick a partner to "own" this repo this week; next week the other partner will own the repo. Once you have forked the repo into your space, open a cygwin terminal window and clone it to your local machine with a command something like:

```
git clone https://your-account@bitbucket.org/your-account/msu-csc232-lab01.git
```

In the above example, be sure to replace `your-account` with your actual Bitbucket account username. All subsequent commands shown in the README are to be issued from this cygwin terminal window. NOTE: Cygwin maps its directories in a curious manner. When you open a Cygwin terminal window, your "home" directory is `/home/your-cygwin-acct-username`. Unfortunately, that does not map to your Windows home directory. From the cygwin perspective, your Windows home directory is located at `/cygdrive/c/users/your-windows-username`. From a Windows perspective, your "cygwin home directory" is located at `C:\cygwin\home\your-cygwin-acct-username` (where `C:\cygwin` is where ever you chose to install cygwin). Take a moment to familiarize yourself with these curious folder mappings in cygwin.

When you issue the `git clone` command, the result is to create a directory that carries the same name as the repo in the directory in which you issued the git command. Thus, after cloning, you'll have to navigate into that newly created directory to work with the source files and/or to issue git commands. Do this now:

```
cd msu-csc232-lab01
```

As usual, once cloned you must create a develop branch within which to conduct your work so that a pull request can be made to review your work before we merge into production. Create this branch as follows:

```
git checkout -b develop
```

Next make sure you're at workable starting point. Compile the code first to ensure it is in expected state:

```
g++ -std=c++14 *.cpp -lcppunit -o Runner
```

(If the `-std=c++14` switch doesn't work, try `-std=c++11`.) This code *should* compile. When it does, you shouldn't see any output, you'll simply return to the command line prompt. Make the executable has in fact been created by listing the contents of the directory with the following command:

```
ls -l
-rwx------+ 1 jdaehn jdaehn    960 Aug 29 00:06 Circle.cpp
-rwx------+ 1 jdaehn jdaehn   1703 Aug 28 23:53 Circle.h
-rwx------+ 1 jdaehn jdaehn   2574 Aug 29 00:14 CircleTest.cpp
-rwx------+ 1 jdaehn jdaehn   1160 Aug 29 00:14 CircleTest.h
-rwx------+ 1 jdaehn jdaehn   1395 Aug 29 00:09 Rectangle.cpp
-rwx------+ 1 jdaehn jdaehn   2353 Aug 29 00:02 Rectangle.h
-rwx------+ 1 jdaehn jdaehn   3365 Aug 29 00:14 RectangleTest.cpp
-rwx------+ 1 jdaehn jdaehn   1420 Aug 29 00:14 RectangleTest.h
-rwx--xr-x+ 1 jdaehn jdaehn 172654 Aug 29 00:39 Runner.exe
-rwx------+ 1 jdaehn jdaehn    593 Aug 28 23:37 Shape.h
-rwx------+ 1 jdaehn jdaehn   2455 Aug 29 00:15 ShapeTestRunner.cpp

```

**Note**: you won't see `jdaehn` but instead you should see your own username. Also, the dates and file sizes may not necessarily be the same. The most import file you should see at this point is `Runner.exe` as this would have been created by a successful compilation and build of your source code.

Next execute the test suite. You should see a whole bunch of output, the most important being the last two lines indicating that all 14 tests have failed.

```
./Runner.exe
CircleTest::testDefaultCircleRadius
 : assertion
CircleTest::testDefaultCircleArea
 : assertion
CircleTest::testDefaultCirclePerimeter
 : assertion
CircleTest::testMutatorSetsDefaultRadiusGivenNegativeInput
 : assertion
CircleTest::testParameterizedCircleArea
 : assertion
CircleTest::testParameterizedCirclePerimeter
 : assertion
RectangleTest::testDefaultRectangleLength
 : assertion
RectangleTest::testDefaultRectangleWidth
 : assertion
RectangleTest::testDefaultRectangleArea
 : assertion
RectangleTest::testDefaultRectanglePerimeter
 : assertion
RectangleTest::testMutatorSetsDefaultLengthGivenNegativeInput
 : assertion
RectangleTest::testMutatorSetsDefaultWidthGivenNegativeInput
 : assertion
RectangleTest::testParameterizedRectangleArea
 : assertion
RectangleTest::testParameterizedRectanglePerimeter
 : assertion
CircleTest.cpp:32:Assertion
Test name: CircleTest::testDefaultCircleRadius
double equality assertion failed
- Expected: 1
- Actual  : 0
- Delta   : 1e-06
- The default radius is expected to be 1.0

CircleTest.cpp:41:Assertion
Test name: CircleTest::testDefaultCircleArea
double equality assertion failed
- Expected: 3.14159265358979
- Actual  : 0
- Delta   : 1e-06
- The default area is expected to equal PI

CircleTest.cpp:50:Assertion
Test name: CircleTest::testDefaultCirclePerimeter
double equality assertion failed
- Expected: 6.28318530717959
- Actual  : 0
- Delta   : 1e-06
- The default perimeter is expected to equal 2 * PI

CircleTest.cpp:60:Assertion
Test name: CircleTest::testMutatorSetsDefaultRadiusGivenNegativeInput
double equality assertion failed
- Expected: 1
- Actual  : 0
- Delta   : 1e-06
- The radius is expected to default to 1.0

CircleTest.cpp:70:Assertion
Test name: CircleTest::testParameterizedCircleArea
double equality assertion failed
- Expected: 19.6349540849362
- Actual  : 0
- Delta   : 1e-06
- The actual area does not equal the expected area

CircleTest.cpp:80:Assertion
Test name: CircleTest::testParameterizedCirclePerimeter
double equality assertion failed
- Expected: 15.707963267949
- Actual  : 0
- Delta   : 1e-06
- The actual perimeter does not equal the expected perimeter

RectangleTest.cpp:32:Assertion
Test name: RectangleTest::testDefaultRectangleLength
double equality assertion failed
- Expected: 2
- Actual  : 0
- Delta   : 1e-06
- actual should equal expected

RectangleTest.cpp:41:Assertion
Test name: RectangleTest::testDefaultRectangleWidth
double equality assertion failed
- Expected: 1
- Actual  : 0
- Delta   : 1e-06
- actual should equal expected

RectangleTest.cpp:50:Assertion
Test name: RectangleTest::testDefaultRectangleArea
double equality assertion failed
- Expected: 2
- Actual  : 0
- Delta   : 1e-06
- actual should equal expected

RectangleTest.cpp:59:Assertion
Test name: RectangleTest::testDefaultRectanglePerimeter
double equality assertion failed
- Expected: 6
- Actual  : 0
- Delta   : 1e-06
- actual should equal expected

RectangleTest.cpp:69:Assertion
Test name: RectangleTest::testMutatorSetsDefaultLengthGivenNegativeInput
double equality assertion failed
- Expected: 2
- Actual  : 0
- Delta   : 1e-06
- actual should equal default length of 2.0

RectangleTest.cpp:79:Assertion
Test name: RectangleTest::testMutatorSetsDefaultWidthGivenNegativeInput
double equality assertion failed
- Expected: 1
- Actual  : 0
- Delta   : 1e-06
- actual should equal default width of 1.0

RectangleTest.cpp:90:Assertion
Test name: RectangleTest::testParameterizedRectangleArea
double equality assertion failed
- Expected: 6
- Actual  : 0
- Delta   : 1e-06
- actual should equal expected

RectangleTest.cpp:101:Assertion
Test name: RectangleTest::testParameterizedRectanglePerimeter
double equality assertion failed
- Expected: 10
- Actual  : 0
- Delta   : 1e-06
- actual should equal expected

Failures !!!
Run: 14   Failure total: 14   Failures: 14   Errors: 0
```

*Stop and ask your lab instructor for help if you were unable to compile and/or run this code at this point.*

Next, browse the source code. Study it carefully; it contains a lot of useful information. We suggest looking at the code in the following order:

1. Shape.h
1. Circle.h
1. Circle.cpp
1. Rectangle.h
1. Rectangle.cpp

## To Do...

Making sure that you are still in your `develop` branch, carry out your TDD process by modifying the source code incrementally so as to get the tests to pass. To see this, just type

```
git branch
* develop
  master
```

The output will list all the branches you have checked out locally and the current branch is highlighted with an asterisk.

When they've all passed, you're all done! All that will be need then is to create a pull request that includes your partner and me as a reviewer.

### More specifically...

Take a look at the `CircleTest.h` unit test specification by opening that file in a text editor. Specifically, look at lines 15-24 that looks like this:

```
CPPUNIT_TEST_SUITE(CircleTest);

    CPPUNIT_TEST(testDefaultCircleRadius);
    CPPUNIT_TEST(testDefaultCircleArea);
    CPPUNIT_TEST(testDefaultCirclePerimeter);
    CPPUNIT_TEST(testMutatorSetsDefaultRadiusGivenNegativeInput);
    CPPUNIT_TEST(testParameterizedCircleArea);
    CPPUNIT_TEST(testParameterizedCirclePerimeter);

CPPUNIT_TEST_SUITE_END();
```

Tackle each of these tests in turn, that is make the modifications necessary to `Circle.cpp` to get the first test (`testDefaultCircleRadius`) to pass. To do this, modify `Circle.cpp` as needed. To see if you've done what's necessary, recompile your code and run the test suite again:

```
g++ -std=c++14 *.cpp -lcppunit -o Runner
./Runner.exe
CircleTest::testDefaultCircleRadius
 : OK
...
```

You know the test has passed when you see `: OK` printed after the name of the test as shown above. Note: The changes, even very simple ones, you make to get a test to pass very well may make other tests pass. The point here is that once you've gotten one (or perhaps more) test(s) to pass, make a commit, push your changes, and move on to the next failing test. Generally speaking, you'll probably want to go through the following cycle of `git` commands after getting your tests to pass:

```
git status
git add <untracked file name>
git commit -m "CSC232-LAB01 - Brief message indicating changes made."
git push
```

NOTE: The first time you do this, you might see the following output:

```
fatal: The current branch develop has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin develop
```

Don't panic; just listen to what they're saying and issue the git command with the `--set-upstream` switch:

```
git push --set-upstream origin develop
```

Generally speaking, you should only have to do that once.

Once you've finished getting the `Circle` and `Rectangle` unit tests to all pass, log onto Bitbucket.org and create a pull request for your work, making sure to add your partner and your instructor as reviewers to the pull request. Creating that pull request is the last step to completing this lab and it must be made before the due date (midnight, Friday, September 2, 2016.)
