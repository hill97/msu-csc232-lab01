/**
 * @file   CircleTest.h
 * @author Jim Daehn
 * @brief  Specification of the Circle Unit Test. DO NOT MODIFY THE CONTENTS 
 * OF THIS FILE! ANY MODIFICATION TO THIS FILE WILL RESULT IN A GRADE OF 0 FOR
 * THIS LAB!
 */

#ifndef CIRCLE_TEST_H
#define CIRCLE_TEST_H

#include <cppunit/extensions/HelperMacros.h>

class CircleTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(CircleTest);

    CPPUNIT_TEST(testDefaultCircleRadius);
    CPPUNIT_TEST(testDefaultCircleArea);
    CPPUNIT_TEST(testDefaultCirclePerimeter);
    CPPUNIT_TEST(testMutatorSetsDefaultRadiusGivenNegativeInput);
    CPPUNIT_TEST(testParameterizedCircleArea);
    CPPUNIT_TEST(testParameterizedCirclePerimeter);

    CPPUNIT_TEST_SUITE_END();

public:
    CircleTest();
    virtual ~CircleTest();
    void setUp();
    void tearDown();

private:
    void testDefaultCircleRadius();
    void testDefaultCircleArea();
    void testDefaultCirclePerimeter();
    void testMutatorSetsDefaultRadiusGivenNegativeInput();
    void testParameterizedCircleArea();
    void testParameterizedCirclePerimeter();
};

#endif /* CIRCLE_TEST_H */

